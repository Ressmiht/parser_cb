#!/bin/env python3

import requests
import xmltodict
import pandas as pd
from datetime import date, timedelta, datetime
import psycopg2 as ps

current_date = date(2023, 8, 10)
while current_date != date.today():
    url = f'https://www.cbr.ru/scripts/XML_daily.asp?date_req={current_date.strftime("%d.%m.%Y")}'
    response = requests.get(url)
    if response.status_code != 200:
        raise Exception("ERROR")
    xml = response.text
    dictionary = xmltodict.parse(xml)
    dictionary.get('ValCurs')
    curr_date = dictionary.get('ValCurs').get('@Date')
    list_curr = dictionary.get('ValCurs').get('Valute')
    lst = []
    for el in list_curr:
        lst.append([datetime.strptime(curr_date, '%d.%m.%Y'), el.get('CharCode'), el.get('Name'),
                    float(el.get('Value').replace(',', '.'))/int(el.get('Nominal'))])

    conn = ps.connect(
        host="localhost",
        port=5432,
        database="postgres",
        user='********',
        password='******')
    cursor = conn.cursor()
    cursor.executemany("INSERT INTO currencies_cbr (date, valute_code, valute_name, currency)"
                       "VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING", lst)
    conn.commit()
    cursor.close()
    conn.close()
    current_date += timedelta(days = 1)